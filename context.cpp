#include "context.h"

context::context(int max_vertices_)
{
    num_symbols = num_looked = num_esc = most_num = 0;
    max_vertices = max_vertices_;
    rnd = new random(42); // that is the key to success
}

context::context(context *l_link, int idx, int max_vertices) : l_link(l_link), idx(idx), max_vertices(max_vertices)
{
    num_symbols = num_looked = num_esc = most_num = 0;
    rnd = new random(42); // that is the key to success
}

context::~context(void)
{
    delete rnd;
}

void context::update(int aggr, int &num_vertex)
{
    num_symbols = 0;
    most_num = 0;
    for (auto it = freq.begin(); it != freq.end();) {
        it->second >>= 1;
        if (!it->second && h_links.count(it->first)) {
            ++it->second;
        }
        num_symbols += it->second;
        most_num = max(most_num, it->second);
        if (it->second == 0) {
            it = freq.erase(it);
            --num_vertex;
        } else {
            ++it;
        }
    }
}

void context::add(uint c, int aggr, int &num_vertex)
{
    if (idx < 0 || c == ppm_FLUSH_symbol)
        return;
    if (!freq.count(c)) {
        if (num_vertex >= max_vertices) {
            if (freq.size()) {
                int r = rnd->rand() % freq.size();
                auto it = freq.begin();
                int k = 0;
                for (; k < r && it != freq.end(); ++it) {
                    ++k;
                }
                if (it->second == most_num) {
                    if (it == freq.begin()) {
                        most_num = 0;
                    } else {
                        --it;
                    }
                }
                num_symbols -= it->second;
                freq.erase(it);
                freq[c] = aggr;
                --num_vertex;
            } else {
                return;
            }
        } else {
            freq[c] = aggr;
            most_num = max(most_num, freq[c]);
        }
    } else {
        --num_vertex;
        freq[c] += aggr;
        most_num = max(most_num, freq[c]);
    }
    ++num_vertex;
    num_symbols += aggr;
    if (num_symbols >= max_freq) {
        update(aggr, num_vertex);
    }
}

inline long long context::get_escD(void)
{
    if ((num_looked - num_esc) * 2 - freq.size() == 0)
        return 1;
    return max(1ULL, num_symbols * freq.size() / ((num_looked - num_esc) * 2 - freq.size()));
    //max(freq.size() / (num_looked * 2), 1ULL);
}

ch_symbol context::get_freq(uint c, bool *used, escape &esc, string &context)
{
    ++num_looked;
    ch_symbol sym(0, 0, 0, false);
    if (idx == -2) { // staff model -- generate FLUSH or EOF
        sym.range = 101;
        sym.high = 100;
        if (c == ppm_EOF_symbol) {
            sym.low = 100;
            ++sym.high;
        }
        return sym;
    }
    if (idx == -1) { // means we are in the root_model and have to use constant frequences = 1
        sym.high = sym.range = 1;
        for (int i = 0; i < ppm_max_symbols; i++) {
            if (i < c) {
                sym.low += !used[i];
                sym.high += !used[i];
            }
            sym.range += !used[i];
            used[i] = false;
        }
        if (c >= ppm_max_symbols) {
            sym.is_esc = true;
        }
        return sym;
    }
    int sign = 0;
    for (auto it = freq.begin(); it != freq.end(); ++it) {
        if (it->first == c) { // we have found our symbol
            sym.high += it->second;
            sign = 1;
        }
        if (!used[it->first]) { // we have not used this symbol in higher orders
            used[it->first] = true;
            if (!sign) {
                sym.low += it->second;
                sym.high += it->second;
            }
            sym.range += it->second;
        }
    }
    long long escape; // if we have no enough information or just use too low ppm ordering simply count escD
    if (context.size() >= 4) {
        escape = esc.get_esc(context, idx, num_esc, num_looked - 1 - num_esc, !sign, 1);
    } else {
        escape = -1;
    }
    if (escape == -1)
        escape = get_escD();
    if (!sign) { // no such symbol in current model -- esc is generated
        sym.high += escape;
        sym.is_esc = true;
        ++num_esc;
    } else {
        memset(used, 0, sizeof(bool) * ppm_max_symbols); // reset used count because we have found our symbol
    }
    sym.range += escape;
    return sym;
}

long long context::get_range(bool *used, escape &esc, string &context)
{
    if (idx == -2) { // staff model -- generate FLUSH or EOF
        return 101; // means that FLUSH is generated about 100 times frequently that EOF
    }
    if (idx == -1) { // means we are in the root_model and have to use constant frequences = 1
        int ans = 1;
        for (int i = 0; i < ppm_max_symbols; i++)
            ans += !used[i];
        return ans;
    }
    ++num_looked;
    long long range = 0;
    for (auto it = freq.begin(); it != freq.end(); ++it) {
        if (!used[it->first]) { // we have not used this symbol in higher orders
            range += it->second;
        }
    }
    long long escape; // if we have no enough information or just use too low ppm ordering simply count escD
    if (context.size() >= 4) {
        escape = esc.get_esc(context, idx, num_esc, num_looked - 1 - num_esc, 1, 0);
    } else {
        escape = -1;
    }
    if (escape == -1)
        escape = get_escD();
    range += escape;
    --num_looked;
    return range;
}

pair <uint, ch_symbol> context::find(long long count, bool *used, escape &esc, string &context)
{
    ++num_looked;
    ch_symbol sym(0, 0, 0, false);
    if (idx == -2) { // staff model -- generate FLUSH or EOF
        sym.range = 101;
        sym.high = 100;
        uint c = ppm_FLUSH_symbol;
        if (count > 100) { // EOF should be generated
            sym.low = 100;
            sym.high = 101;
            c = ppm_EOF_symbol;
        }
        return mp(c, sym);
    }
    if (idx == -1) { // means we are in the root_model and have to use constant frequences == 1
        uint c;
        for (int i = 0; i < ppm_max_symbols; i++) {
            if (!used[i]) { // we have not used this symbol in higher orders
                if (sym.high <= count) {
                    ++sym.low;
                    ++sym.high;
                    if (sym.high > count)
                        --sym.low;
                    c = i;
                }
                ++sym.range;
            }
            used[i] = false;
        }
        if (sym.high <= count) { // no such symbol in current model -- esc is generated
            ++sym.high;
            sym.is_esc = true;
            c = ppm_ESC_symbol;
        }
        ++sym.range;
        return mp(c, sym);
    }
    int sign = 0;
    uint c;
    for (auto it = freq.begin(); it != freq.end(); ++it) {
        if (!used[it->first]) { // we have not used this symbol in higher orders
            used[it->first] = true;
            if (sym.high <= count) {
                sym.low += it->second;
                sym.high += it->second;
                if (sym.high > count)
                    sym.low -= it->second;
                c = it->first;
            }
            sym.range += it->second;
        }
    }
    long long escape; // if we have no enough information or just use too low ppm ordering simply count escD
    if (context.size() >= 4) {
        escape = esc.get_esc(context, idx, num_esc, num_looked - 1 - num_esc, sym.high <= count, 1);
    } else {
        escape = -1;
    }
    if (escape == -1)
        escape = get_escD();
    if (sym.high <= count) { // no such symbol in current model -- esc is generated
        sym.high += escape;
        sym.is_esc = true;
        c = ppm_ESC_symbol;
        ++num_esc;
    } else {
        memset(used, 0, sizeof(bool) * ppm_max_symbols); // reset used count because we have found our symbol
    }
    sym.range += escape;
    return mp(c, sym);
}