#ifndef ARITHMETIC_H
#define ARITHMETIC_H

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <iostream>

#include "buffer.h"
#include "table.h"
#include "CntTree.h"

#define top_value ((1ll << 32ll) - 1ll)
#define qtr1 (top_value / 4ll + 1ll)
#define half (2ll * qtr1)
#define qtr3 (3ll * qtr1)

#define v_num_update 500000
#define v_ari_update 500000

class Arithmetic
{
public:
    Arithmetic();
    ~Arithmetic(void);
    void coding(char *mode, int a, char *inFileName, char *outFileName, int max_memory = 3);
    void decoding(char *inFileName, char *outFileName);
    uint wSize;
    uint size;
private:
    int symbol[max_symbols], index[max_symbols];
    int memory_table[4];
    int max_order;
    table *freq;
    CntTree *ppm;
    long long low;
    long long high;
    long long value;
    int update_num;
    int update_perc;
    int good_count;
    int bad_count;
    uint aggr;
    uint progress;
    uint percentage;
    int mode;
    FILE *rFile, *wFile;
    void initFrequency(void);
    void ari_encode(uint c);
    uint ari_decode(void);
    void encode(ch_symbol sym);
    void decode(ch_symbol sym);
    void ppm_encode(uint c);
    uint ppm_decode();
    void final_encode(void);
    void updateFrequency(uint sb);
    void updateContext(uint c);
    void output_progress(void);
    void drop_bits(void);
    bool compute_ratio(void);
    Buffer *buf;
};

#endif // ARITHMETIC_H