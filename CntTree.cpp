#include "CntTree.h"

CntTree::CntTree(int aggr_,  int max_vertices_, int max_order) : max_order(max_order)
{
    max_memory = max_vertices_;
    aggr = aggr_;
    // create staff model for FLUSH and EOF symbols
    staff_model = new context(NULL, -2, max_memory);
    staff_model->num_symbols = 2;
    // create fixed model with constant frequences
    fixed_model = new context(staff_model, -1, max_memory);
    fixed_model->num_symbols = ppm_max_symbols;
    // create root model - just like usual arithmetic
    root_model = new context(fixed_model, 0, max_memory);
    root_model->num_symbols = 0;

    current_model = root_model;
    memset(used, 0, sizeof(used));
    num_vertices = 0;
    cnt = "";
}

CntTree::~CntTree(void)
{
    free_tree(root_model);
    delete staff_model;
    delete fixed_model;
}

void CntTree::free_tree(pcontext model)
{
    if (!model) // just in case ;)
        return;
    for (auto it = model->h_links.begin(); it != model->h_links.end(); it++) {
        free_tree(it->second);
    }
    delete model;
}

pair <uint, ch_symbol> CntTree::find(long long count)
{
    pair <uint, ch_symbol> tmp;
    tmp = current_model->find(count, used, esc_probs, cnt);
    if (tmp.first != ppm_ESC_symbol) {
        current_model->add(tmp.first, aggr, num_vertices); // adding aggr to sym number and flushing model if necessary
    }
    return tmp;
}

void CntTree::update_up(uint c)
{
    if (current_model == fixed_model || current_model == staff_model) {
        current_model = root_model;
        current_model->add(c, aggr, num_vertices); // adding aggr to sym number and flushing model if necessary
    }
    pcontext tmp = current_model;
    for (int i = tmp->idx; i < cnt.size(); i++) {
        if (current_model->h_links.count((unsigned char)cnt[i])) {
            current_model = current_model->h_links[(unsigned char)cnt[i]];
            current_model->add(c, aggr, num_vertices); // adding aggr to sym number and flushing model if necessary
        } else {
            break;
        }
    }
}

void CntTree::update_all(pcontext model)
{
    if (!model) // just in case ;)
        return;
    for (auto it = model->h_links.begin(); it != model->h_links.end();) {
        update_all(it->second);
        if (it->second->freq.size() == 0 && it->second->h_links.size() == 0) { // delete empty models
            delete it->second;
            it = model->h_links.erase(it);
        } else {
            ++it;
        }
    }
    model->update(aggr, num_vertices);
}

long long CntTree::get_range(void)
{
    return current_model->get_range(used, esc_probs, cnt);
}

ch_symbol CntTree::get(uint c)
{
    ch_symbol sym = current_model->get_freq(c, used, esc_probs, cnt);
    if (!sym.is_esc) // that is done to make it equal with decompression algorithm (we will update everything later)
        current_model->add(c, aggr, num_vertices); // adding aggr to sym number and flushing model if necessary
    return sym;
}

void CntTree::reduce_order()
{
    current_model = current_model->l_link;
}

void CntTree::set_model()
{
    current_model = root_model;
    int i = 0;
    for (int i = 0; i < cnt.size(); i++) {
        if (!current_model->h_links.count((unsigned char)cnt[i])) { // create context if it doesn't exist
            if (num_vertices >= max_memory) // no memory.. :(
                return;
            current_model->h_links[(unsigned char)cnt[i]] = new context(current_model, current_model->idx + 1, max_memory);
            if (current_model->freq.count((unsigned char)cnt[i]) == 0) {
         //       current_model->add((unsigned char)cnt[i], aggr, num_vertices);
            }
        }
        current_model = current_model->h_links[(unsigned char)cnt[i]];
    }
}