#include "table.h"

table::table()
{
    memset(freq.a, 0, sizeof(freq.a));
    memset(freq.b, 0, sizeof(freq.b));
    for (int i = 0; i < max_symbols; i++) {
        freq.add(i, 1);
    }
}

long long table::get(int i)
{
    return freq.get(i);
}

void table::update(int i, unsigned int aggr)
{
   // if (i != FLUSH_symbol)
        freq.add(i, aggr);
    if (freq.get(EOF_symbol) >= max_freq || i == FLUSH_symbol) {
        freq.update();
    }
}

table::~table(void)
{
}
