#include "arithmetic.h"
#include <ctime>

enum {
    a_number = 16
};

Arithmetic archive[a_number];
unsigned int result[a_number];
char str[100];

void copy_file(char *name1, char *name2)
{
    FILE *fr = fopen(name1, "rb");
    FILE *fw = fopen(name2, "wb");
    unsigned int c;
    while ((c = fgetc(fr)) != EOF) {
        fputc(c, fw);
    }
    fclose(fr);
    fclose(fw);
}

int main(int argc, char *argv[]) {
    clock_t time = clock();
    if (argc < 4) {
        fprintf(stderr, "Syntactic error: too few arguments for the program.\n");
        return 1;
    }
    if (argc > 6) {
        fprintf(stderr, "Syntactic error: too many arguments for the program.\n");
        return 1;
    }
    if (!strcmp(argv[2], argv[3])) {
        fprintf(stderr, "Error!! File names must be distinct!\n");
        return 0;
    }
    FILE *fw;
    if (fw = fopen(argv[3], "rb")) {
        fprintf(stderr, "File \"%s\" exists in current directory. File has been rewritten\n", argv[3]);
        fclose(fw);
        remove(argv[3]);
    }
    if (!strcmp(argv[1], "c")) {
        if (argc == 4 || strcmp(argv[4], "ppm")) {
            // launching compressing a_number times with different aggressions
            #pragma omp parallel for
            for (int a = 0; a < a_number; ++a) {
                char outFile[100], cpyFile[100];
                sprintf(outFile, "w%d", a);
                sprintf(cpyFile, "cpy%d", a);
                copy_file(argv[2], cpyFile);
                archive[a].coding(((argc == 4) ? "ari" : argv[4]), a, cpyFile, outFile);
                result[a] = archive[a].wSize;
                remove(cpyFile);
            }
            // choosing minimum file size and delete all trash
            int mn = *min_element(result, result + a_number), tmp;
            tmp = mn;
            for (int i = 0; i < a_number; i++) {
                sprintf(str, "w%d", i);
                if (result[i] == mn) {
                    rename(str, argv[3]);
                    if (archive[i].size <= archive[i].wSize) {
                        fw = fopen(argv[3], "wb");
                        FILE *fr = fopen(argv[2], "rb");
                        unsigned int c = 1;
                        fputc(c, fw);
                        while ((c = fgetc(fr)) != EOF) {
                            fputc(c, fw);
                        }
                        fclose(fw);
                        fclose(fr);
                        archive[i].wSize = archive[i].size;
                    }
                    fprintf(stderr, "Compression completed\nSource size: %u\nArchive size: %u\nRatio: %.5f\nTime elapsed: %.2f\n", \
                        archive[i].size, archive[i].wSize, (archive[i].wSize * 1.0) / archive[i].size, (double)time / CLOCKS_PER_SEC);
                    --mn;
                } else {
                    remove(str);
                }
            }
        } else {
            int mem = 3;
            if (argc > 5) {
                sscanf(argv[5], "-gb=%d", &mem);
                --mem;
                if (mem < 0 || mem > 3) {
                    fprintf(stderr, "In the last parameter: \"gb=x\", x can be only 1..4\n");
                    return 1;
                }
            }
            archive[0].coding(argv[4], 0, argv[2], argv[3], mem);
            if (archive[0].size <= archive[0].wSize) {
                fw = fopen(argv[3], "wb");
                FILE *fr = fopen(argv[2], "rb");
                unsigned int c = 1;
                fputc(c, fw);
                while ((c = fgetc(fr)) != EOF) {
                    fputc(c, fw);
                }
                fclose(fw);
                fclose(fr);
                archive[0].wSize = archive[0].size;
            }
            time = clock() - time;
            fprintf(stderr, "Compression completed\nSource size: %u\nArchive size: %u\nRatio: %.5f\nTime elapsed: %.2f\n", \
                archive[0].size, archive[0].wSize, (archive[0].wSize * 1.0) / archive[0].size, (double)time / CLOCKS_PER_SEC);
        }
        return 0;
    }
    if (!strcmp(argv[1], "d")) {
        archive[0].decoding(argv[2], argv[3]);
        time = clock() - time;
        fprintf(stderr, "Decompression completed\nTime elapsed: %.2f\n\n", (double)time / CLOCKS_PER_SEC);
        return 0;
    }
    fprintf(stderr, "Syntactic error: you should write 'c' for compressing or 'd' for decompressing as a first argument!!\n");
    return 1;
}