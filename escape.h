#pragma once

#include <unordered_map>
#include <cmath>
using namespace std;

typedef unsigned int uint;

#define max_esc_total 1073741824

struct escape_table
{
    uint esc_count;
    uint total_count;
    escape_table() {}
    escape_table(uint e, uint t) : esc_count(e), total_count(t) {}
};

class escape
{
public:
    escape(void);
    ~escape(void);
    long long get_esc(string &context, int idx, int esc_count, int match_count, bool if_esc, bool if_upd);
private:
    unordered_map <int, escape_table> order[3];
};

