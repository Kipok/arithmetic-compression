#pragma once
#include <unordered_map>
#include "escape.h"
using namespace std;

typedef unsigned int uint;

#define ppm_max_symbols 257
#define ppm_EOF_symbol 257
#define ppm_FLUSH_symbol 258
#define ppm_ESC_symbol 256
#define mp make_pair
#define max_freq 1073741824

struct ch_symbol
{
    long long low;
    long long high;
    long long range;
    bool is_esc;
    ch_symbol(long long low, long long high, long long range, bool is_esc) : low(low), high(high), range(range), is_esc(is_esc) {}
    ch_symbol() {}
};

class random
{
public:
    random(uint seed) : next(seed) {}
    ~random() {}
    uint rand(void) {
        next = next * 1103515245 + 12345;
        return (unsigned int)(next / 65536) % 32768;
    }
private:
    uint next;
};

class context
{
public:
    context(int max_vertices); // 4 gigabytes
    context(context *l_link, int idx, int max_vertices);
    ~context();
    int idx; // order of the context
    int num_symbols;
    int num_looked;
    int num_esc;
    int most_num;
    int max_vertices;
    unordered_map <uint, int> freq; // that is unlike usual table just number of such symbols
    unordered_map <uint, context *> h_links; // links to the higher contexts
    context *l_link; // link to the parent contexts
    void update(int aggr, int &num_vertex);
    void add(uint c, int aggr, int &num_vertex);
    ch_symbol get_freq(uint c, bool *used, escape &esc, string &context);
    long long get_range(bool *used, escape &esc, string &context);
    pair <uint, ch_symbol> find(long long count, bool *used, escape &esc, string &context);
private:
    long long get_escD(void);
    random *rnd;
};