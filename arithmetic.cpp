#include "arithmetic.h"

Arithmetic::Arithmetic()
{
    low = 0;
    high = top_value;
    value = 0;
    aggr = 1;
    percentage = progress = 0;
    memory_table[0] = 1600000LL; // 1 gigabyte  of memory
    memory_table[1] = 3300000LL; // 2 gigabytes of memory
    memory_table[2] = 5300000LL; // 3 gigabytes of memory
    memory_table[3] = 7100000LL; // 4 gigabytes of memory
    good_count = bad_count = 0;
    max_order = 6;
}

Arithmetic::~Arithmetic() 
{
    delete buf;
}

void Arithmetic::output_progress() {
    if (mode) {
        ++progress;
        if ((uint)((progress * 100.0) / (size * 1.0)) > percentage) {
            percentage = (uint)((progress * 100.0) / (size * 1.0));
            fprintf(stderr, "%u%%", percentage);
            if (percentage != 100) {
                if ((progress * 100.0) / (size * 1.0) >= 10.0) {
                    fputc('\b', stderr);
                }
                fputc('\b', stderr);
                fputc('\b', stderr);
            } else {
                fputc('\n', stderr);
            }
        }
    }
}

void Arithmetic::initFrequency()
{
    // matching symbols with its indexes
    for (int i = 1; i < max_symbols; i++) {
        index[i] = max_symbols - i;
        symbol[max_symbols - i] = i;
    }
}

bool Arithmetic::compute_ratio()
{
   if (100.0 * buf->cur_out_size / buf->cur_in_size >= update_perc) {
        buf->cur_in_size = buf->cur_out_size = 0;
        ++bad_count;
        good_count = 0;
        if (bad_count < 20 && (mode == 1) ? (ppm->num_vertices > v_num_update) : (freq->get(EOF_symbol) > v_ari_update)) {
            return true;
        } else {
            return false;
        }
    }
    ++good_count;
    bad_count = 0;
    buf->cur_in_size = buf->cur_out_size = 0;
    return false;
}

void Arithmetic::final_encode() 
{
    // add a few bits to complete final byte and get an appropriate precision
    ++buf->bits_to_follow;
    if (low < qtr1) 
        buf->bit_plus_follow(0); 
    else 
        buf->bit_plus_follow(1);
    fputc(buf->buffer >> buf->bits_to_go, wFile);
}

void Arithmetic::updateContext(uint c)
{
    if (ppm->cnt.size() == max_order)
        ppm->cnt.resize(ppm->cnt.size() - 1);
    ppm->cnt.insert(ppm->cnt.begin(), c);
}

void Arithmetic::coding(char *cmode, int a, char *inFileName, char *outFileName, int max_memory)
{
    mode = !strcmp("ppm", cmode) ? 1 : 0;
    aggr = 1 << a;
    if (!(rFile = fopen(inFileName, "rb"))) {
        fprintf(stderr, "ERROR: File %s doesn't exist in current directory..\n", inFileName);
        exit(0);
    }
    // memorising file size in order to output progress
    fseek(rFile, 0, SEEK_END);
    size = ftell(rFile);
    fseek(rFile, 0, SEEK_SET);
    if (size < 20 * (1 << 20)) { // huge bin files
        update_num = 256;
        update_perc = 80;
        max_order = 6;
    }
    if (size < 9 * (1 << 20)) { // usual bin files
        update_num = 256;
        update_perc = 80;
        max_order = 5;
    }
    if (size < 3000000) { // small files -- no sense in the update
        update_num = 256;
        update_perc = 80;
        max_order = 6;
    }
    if (mode) {
        fprintf(stderr, "progress --> ");
    }
    freq = new table();
    ppm = new CntTree(aggr, memory_table[max_memory], max_order);
    wFile = fopen(outFileName, "wb");
    // write an aggression in the beginning of the file and its last bit shows if we used ppm or no
    a <<= 1;
    a += mode;
    a <<= 2;
    a += max_memory;
    a <<= 1; // this bit is used if we can not compress file => just leave it as it is
    fputc(a, wFile);
    fputc(max_order, wFile);
    // write 
    buf = new Buffer(rFile, wFile);
    buf->bits_to_go = 8; 
    // generate frequences' table
    initFrequency();    
    // start of coding
    uint c;
    while ((c = fgetc(rFile)) != EOF) {
        buf->cur_in_size += 8;
        if (!mode) { // without ppm
            ari_encode(c);
            freq->update(symbol[c], aggr);
            if (buf->cur_in_size == update_num * 8 && compute_ratio()) {
                ari_encode(FLUSH_symbol);
                freq->update(symbol[FLUSH_symbol], aggr);
            }
        } else {
            ppm_encode(c);
            updateContext(c);
            if (buf->cur_in_size == update_num * 8 && compute_ratio()) {
                ppm_encode(ppm_FLUSH_symbol);
                ppm->update_all(ppm->root_model);
            }
        }
        output_progress();
    }
    if (!mode) {
        ari_encode(EOF_symbol);
    } else {
        ppm_encode(ppm_EOF_symbol);
    }
    final_encode();

    fseek(wFile, 0, SEEK_END);
    wSize = ftell(wFile);

    fclose(rFile);
    fclose(wFile);
    delete freq;
    delete ppm;
}

void Arithmetic::decoding(char *inFileName, char *outFileName) 
{
    if (!(rFile = fopen(inFileName, "rb"))) {
        fprintf(stderr, "ERROR: File %s doesn't exist in current directory..\n", inFileName);
        exit(0);
    }
    aggr = fgetc(rFile);
    if (aggr & 1) {
        wFile = fopen(outFileName, "wb");
        uint c;
        while ((c = fgetc(rFile)) != EOF) {
            fputc(c, wFile);
        }
        fclose(wFile);
        fclose(rFile);
        return;
    } else
        aggr >>= 1;
    int max_memory = aggr & 3;
    aggr >>= 2;
    mode = aggr & 1;
    aggr = 1 << (aggr >> 1);
    max_order = fgetc(rFile);
    freq = new table();
    ppm = new CntTree(aggr, memory_table[max_memory], max_order);
    wFile = fopen(outFileName, "wb");
    buf = new Buffer(rFile, wFile);
    buf->bits_to_go = 0;
    initFrequency();    
    // initialising value by first bits from the archive
    for (int i = 0; i < max_bit; i++) {
        value <<= 1;
        value += buf->input_bit();
    }
    // decoding
    while (true) {
        uint c;
        if (!mode) { // without ppm
            c = ari_decode();
            freq->update(symbol[c], aggr);
            if (c == EOF_symbol)
                break;
            if (c != FLUSH_symbol)
                fputc(c, wFile);
        } else {
            c = ppm_decode(); // the handler of the flush symbol is inside
            if (c != ppm_FLUSH_symbol)
                updateContext(c);
            if (c == ppm_EOF_symbol)
                break;
            if (c != ppm_FLUSH_symbol)
                fputc(c, wFile);
        }
    }
    fclose(rFile);
    fclose(wFile);
    delete freq;
    delete ppm;
}

void Arithmetic::drop_bits(void)
{
    // trying to drop few bits that we already know
    while (true) {
        if (high < half) {
            buf->bit_plus_follow(0);
        } else {
            if (low >= half) {
                buf->bit_plus_follow(1);
                low -= half;
                high -= half;
            } else {
                if (low >= qtr1 && high < qtr3) {
                    low -= qtr1;
                    high -= qtr1;
                    ++buf->bits_to_follow;
                } else {
                    break;
                }
            }
        }
        low <<= 1;
        high = (high << 1) + 1;
    }
}

void Arithmetic::encode(ch_symbol sym) 
{
    // scale current segment to the segment of symbol c
    long long range = high - low + 1ll;
    high = low + range * sym.high / sym.range - 1ll;
    low = low + range * sym.low / sym.range;
    drop_bits();
}

void Arithmetic::ppm_encode(uint c)
{
    ppm->set_model();
    if (USE_LOE) { // looking for the best order of ppm for the current symbol
        if (ppm->current_model != ppm->root_model) {
            while (1ll * ppm->current_model->most_num * ppm->current_model->l_link->num_symbols < 1ll * ppm->current_model->l_link->most_num * ppm->current_model->num_symbols) {
                ppm->reduce_order();
            }
        }
    }
    while (ppm->current_model != ppm->fixed_model && ppm->current_model->freq.size() == 0) // don't use models with no symbols
        ppm->reduce_order();
    ch_symbol sym = ppm->get(c);
    while (sym.is_esc) {
        encode(sym);
        ppm->reduce_order();
        sym = ppm->get(c);
    }
    encode(sym);
    ppm->update_up(c);
}

void Arithmetic::ari_encode(uint c)
{
    encode(ch_symbol(freq->get(symbol[c] - 1), freq->get(symbol[c]), freq->get(EOF_symbol), false));
}

void Arithmetic::decode(ch_symbol sym)
{
    long long range = high - low + 1ll;
    // scaling sequence
    high = low + range * sym.high / sym.range - 1ll;
    low = low + range * sym.low / sym.range;
    // doing the same thing that is in the encode procedure without dropping bits
    while (true) {
        if (high < half) {
        } else {
            if (low >= half) {
                low -= half;
                high -= half;
                value -= half;
            } else {
                if (low >= qtr1 && high < qtr3) {
                    low -= qtr1;
                    high -= qtr1;
                    value -= qtr1;
                } else {
                    break;
                }
            }
        }
        high = (high << 1ll) + 1;
        value = (value << 1ll) + buf->input_bit();
        low <<= 1ll;
    }
}

uint Arithmetic::ari_decode() 
{
    long long range = high - low + 1ll;
    // finding frequence for current symbol using binsearch
    long long f_curr = ((value - low + 1ll) * freq->get(EOF_symbol) - 1ll) / range;
    int ind = 0, l = 0, r = EOF_symbol;
    // finding the first frequency that is higher than f_curr
    while (l < r) {
        int m = (l + r) / 2;
        if (freq->get(m) >= f_curr)
            r = m;
        else
            l = m + 1;
    }
    ind = l;
    if (freq->get(l) == f_curr)
        ++ind;
    decode(ch_symbol(freq->get(ind - 1), freq->get(ind), freq->get(EOF_symbol), false));
    return index[ind];
}

uint Arithmetic::ppm_decode()
{
    ppm->set_model();
    if (USE_LOE) { // looking for the best order for ppm for current symbol
        if (ppm->current_model != ppm->root_model) {
            while (1ll * ppm->current_model->most_num * ppm->current_model->l_link->num_symbols < 1ll * ppm->current_model->l_link->most_num * ppm->current_model->num_symbols) {
                ppm->reduce_order();
            }
        }
    }
    while (ppm->current_model != ppm->fixed_model && ppm->current_model->freq.size() == 0) // don't use models with no symbols
        ppm->reduce_order();
    long long range = high - low + 1ll;
    long long f_curr = ((value - low + 1ll) * ppm->get_range() - 1ll) / range;

    pair <uint, ch_symbol> c = ppm->find(f_curr);
    while (c.first == ppm_ESC_symbol) {
        decode(c.second);
        ppm->reduce_order();
        range = high - low + 1ll;
        f_curr = ((value - low + 1ll) * ppm->get_range() - 1ll) / range;
        c = ppm->find(f_curr);
    }
    decode(c.second);
    if (c.first != ppm_FLUSH_symbol) // just because update doesn't affect on flush symbol
        ppm->update_up(c.first);
    else
        ppm->update_all(ppm->root_model);
    return c.first;
}