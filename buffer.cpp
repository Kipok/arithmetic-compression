#include "buffer.h"

Buffer::Buffer(FILE *rfile, FILE *wfile)
{
    this->wFile = wfile;
    this->rFile = rfile;
    buffer = 0;
    bits_to_follow = 0;
    aEOF_bits = 0;
    cur_in_size = cur_out_size = 0;
}

void Buffer::output_bit(int bit) 
{
    buffer >>= 1;
    if (bit) 
        buffer |= 0x80;
    --bits_to_go;
    if (!bits_to_go) {
        fputc(buffer, wFile);
        bits_to_go = 8;
    }
    ++cur_out_size;
}

int Buffer::input_bit() 
{
    int t;
    if (!bits_to_go) {
        buffer = fgetc(rFile);
        if (buffer==EOF) {
            ++aEOF_bits;
            if (aEOF_bits > max_bit - 2) {
                fprintf(stderr,"Bad archive\n");
                exit(0);
            }
        }
        bits_to_go = 8;
    }
    t = buffer & 1;
    buffer >>= 1;
    --bits_to_go;
    return t;
}

void Buffer::bit_plus_follow(int bit) {
    output_bit(bit);
    while (bits_to_follow) {
        output_bit(!bit);
        --bits_to_follow;
    }
}

Buffer::~Buffer() 
{

}