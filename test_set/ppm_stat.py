﻿ #-*- coding: utf-8 -*-
import os, sys, subprocess, shutil, xlwt, xlutils, filecmp

if (not os.path.exists("compressed")):
	os.makedirs("compressed")

fileNames = ["gogol.txt", "idiot.txt", "p&w.txt", "martin-2.txt", "martin-3.txt", "martin-5.txt", "big.dll", "st.bin", "bak.bak", "pe_small.aps", "pe.vspx", "sa.bin", "tokens.dat", "test.dll"]

# execute program and check files
for i in fileNames:
	print("Running file \"" + str(i) + "\"")
	source  = "files/" + str(i)
	archive = "compressed/" + os.path.splitext(str(i))[0] + ".cmp"
	result  = "d" + str(i)
	subprocess.check_call(["../x64/Release/arithmetic.exe", "c", source, archive, "ppm"])
#	subprocess.check_call(["../x64/Release/arithmetic.exe", "d", archive, result])
#	if (filecmp.cmp(source, result) == False):
#		print("File \"" + result + "\" doesn't match its source file")
#		sys.exit()
#	else:
#		os.remove(result)
	
# write to excel
wb_ari = xlwt.Workbook(encoding='utf-8')
wl  = wb_ari.add_sheet(u"result")
pos = 1; whole_ratio = 0; whole_src = 0; rus_rat = 0; eng_rat = 0; bin_rat = 0

wl.write(1, 0, "src_size")
wl.write(2, 0, "cmp_size")
wl.write(3, 0, "ratio")

for i in fileNames:
	size = os.path.getsize("compressed/" + os.path.splitext(str(i))[0] + ".cmp")
	source_size = os.path.getsize("files/" + str(i))
	wl.write(0, pos, str(i))
	wl.write(1, pos, float(source_size))
	wl.write(2, pos, float(size))
	ratio = float(size) / float(source_size)
	if pos < 4:
		rus_rat += ratio
	elif pos < 7:
		eng_rat += ratio
	else:
		bin_rat += ratio
	whole_ratio += ratio
	wl.write(3, pos, float(ratio))
	pos = pos + 1

wl.write(7, 1, "overall")	
wl.write(8, 0, "rus_ratio")
wl.write(9, 0, "eng_ratio")
wl.write(10, 0, "bin_ratio")
wl.write(11, 0, "whole_ratio")
wl.write(8, 1, float(rus_rat) / 3.0)
wl.write(9, 1, float(eng_rat) / 3.0)
wl.write(10, 1, float(bin_rat) / 8.0)
wl.write(11, 1, float(whole_ratio) / 14.0)
	
wb_ari.save("ppm_stat.xls")

toGit = "ppm:\n    RUS: " + str(float(rus_rat) / 3.0) + "\n    ENG: " + str(float(eng_rat) / 3.0) + \
"\n    BIN: " + str(float(bin_rat) / 8.0) + "\n    ALL: " + str(float(whole_ratio) / 14.0) + "\n"
from Tkinter import Tk
r = Tk()
r.withdraw()
r.clipboard_clear()
r.clipboard_append(toGit)
r.destroy()

print("\nGit info has been copied to clipboard")