#include "escape.h"

escape::escape(void)
{
}

escape::~escape(void)
{
}

inline int idnt(uint c)
{
    return (c & 0x60) >> 5;
}

inline double log2(double n)  
{  
    if (n <= 1)
        return 0;
    return 4096 * log(n) / log(2);  
}

long long escape::get_esc(string &context, int idx, int esc_count, int match_count, bool if_esc, bool if_upd)
{
    if (esc_count > 3) {
        return -1; // means we should use normal ppm escape estimation
    }
    int hash[3];
    idx >>= 1;
    const int match_count_map[13] = {0, 1, 2, 3, 3, 4, 4, 5, 5, 5, 6, 6, 6};
    if (match_count > 12) {
        match_count = 7;
    } else {
        match_count = match_count_map[match_count];
    }
    // calculate hashes
    hash[0] = (esc_count << 5) + (match_count << 2) + idx; // 7 bits
    hash[1] = (hash[0] << 8) + (idnt(context[0]) << 6) + (idnt(context[1]) << 4) + (idnt(context[2]) << 2) + idnt(context[3]); // 15 bits   
    hash[2] = (hash[0] << 9) + ((context[0] & 0x7F) << 2) + idnt(context[1]); // 16 bits
    int w[3], t[3], e[3];
    long long total = 0, esc_prob = 0;
    for (int i = 0; i < 3; i++) {
        if (!order[i].count(hash[i])) // have not used it before
            order[i][hash[i]] = escape_table(0, 0);
        t[i] = order[i][hash[i]].total_count;
        if (!t[i])
            ++t[i];
        e[i] = order[i][hash[i]].esc_count;
        w[i] = (t[i] * 4096) / (t[i] * log2(t[i]) - e[i] * log2(e[i]) - (t[i] - e[i]) * log2(t[i] - e[i]) + 1);
        total += w[i];
        esc_prob += e[i] * w[i] / t[i];
    }
    if (esc_prob >= total)
        esc_prob = total - 1;
    if (esc_prob < 1)
        esc_prob = 1;
    if (!if_upd)
        return esc_prob;
    // update
    for (int i = 0; i < 3; i++) {
        order[i][hash[i]].total_count += 20;
        order[i][hash[i]].esc_count += 19 * if_esc;
        if (order[i][hash[i]].total_count >= max_esc_total) {
            order[i][hash[i]].total_count >>= 1;
            order[i][hash[i]].esc_count >>= 1;
        }
    }
    return esc_prob;
}