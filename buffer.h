#ifndef BUFFER_H
#define BUFFER_H

#include <iostream>
#include <cmath>
#include <cstdio>
#include <cstdlib>
using namespace std;

#define max_bit 32ll

class Buffer
{
public:
    Buffer(FILE *rfile, FILE *wfile);
    ~Buffer(void);
    void bit_plus_follow(int bit);
    int input_bit();
    int bits_to_follow;
    int bits_to_go;
    long long cur_in_size;
    long long cur_out_size;
    unsigned int buffer;
private:
    int aEOF_bits;
    void output_bit(int bit);
    FILE *rFile, *wFile;
};

#endif // BUFFER_H