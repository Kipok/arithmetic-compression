#pragma once
#include "context.h"

#define USE_LOE 1
typedef context* pcontext;

class CntTree
{
public:
    CntTree(int aggr_,  int max_vertices_, int max_order = 6);
    ~CntTree(void);
    ch_symbol get(uint c);
    void reduce_order(void);
    void set_model();
    pair <uint, ch_symbol> find(long long count);
    long long get_range(void);
    void update_up(uint c);
    void update_all(pcontext model);
    pcontext fixed_model;
    pcontext staff_model;
    pcontext root_model;
    pcontext current_model;
    int num_vertices;
    int max_memory;
    escape esc_probs;
    string cnt;
    int max_order;
private:
    int aggr;
    bool used[ppm_max_symbols];
    void free_tree(pcontext model);
};